package com.thoughtworks.DailyNeedsOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyNeedsOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyNeedsOneApplication.class, args);
	}

}
