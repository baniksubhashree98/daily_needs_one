package com.thoughtworks.DailyNeedsOne;

import com.thoughtworks.DailyNeedsOne.entity.Product;
import com.thoughtworks.DailyNeedsOne.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSeeder {

    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository) {
        return args -> {
            Product toothpaste = Product.builder()
                    .productId(1)
                    .name("toothpaste")
                    .priceInRupees(75)
                    .build();

            if (productRepository.findById(1).isEmpty())
                productRepository.save(toothpaste);

            Product sunglasses = Product.builder()
                    .productId(2)
                    .name("sunglasses")
                    .priceInRupees(450)
                    .build();

            if (productRepository.findById(2).isEmpty())
                productRepository.save(sunglasses);

            Product torchlight = Product.builder()
                    .productId(3)
                    .name("torchlight")
                    .priceInRupees(80)
                    .build();
            if (productRepository.findById(3).isEmpty())
                productRepository.save(torchlight);
        };
    }

}


