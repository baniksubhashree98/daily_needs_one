package com.thoughtworks.DailyNeedsOne.controller;

import com.thoughtworks.DailyNeedsOne.entity.Version;
import com.thoughtworks.DailyNeedsOne.service.VersionService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RestController
public class VersionController {
    @Autowired
    private VersionService versionService;

    @Bean
    public WebMvcConfigurer configure() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins("http:localhost:3000");
            }
        };
    }
    @CrossOrigin(origins="http://localhost:3000")
    @GetMapping("/version")
    public ResponseEntity<?> getCurrentVersion() {
        String currentVersion = versionService.getCurrentVersion();
        JSONObject response = new JSONObject();
        response.put("CurrentVersion", currentVersion);
        return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
    }
}
