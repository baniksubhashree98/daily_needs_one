package com.thoughtworks.DailyNeedsOne.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "versions")
public class Version {

    @Id
    @JsonProperty
    private String versionName;

    @Column(nullable = false)
    @JsonProperty
    private Boolean isCurrent;

}



