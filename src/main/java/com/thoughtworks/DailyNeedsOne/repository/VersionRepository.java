package com.thoughtworks.DailyNeedsOne.repository;

import com.thoughtworks.DailyNeedsOne.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionRepository extends JpaRepository<Version, String> {
    //    @Query(value = "select * from versions v where v.is_current = TRUE", nativeQuery = true)
    @Query(value = "select v.version_name from versions v where v.is_current = TRUE", nativeQuery = true)
    String getCurrentVersion();
}