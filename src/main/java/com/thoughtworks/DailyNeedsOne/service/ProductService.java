package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Product;
import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
}
