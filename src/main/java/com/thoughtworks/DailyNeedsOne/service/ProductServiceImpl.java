package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Product;
import com.thoughtworks.DailyNeedsOne.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

}
