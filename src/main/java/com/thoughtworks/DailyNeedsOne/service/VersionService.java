package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Version;

public interface VersionService {
    String getCurrentVersion();
}
