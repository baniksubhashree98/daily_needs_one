package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Version;
import com.thoughtworks.DailyNeedsOne.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class VersionServiceImpl implements VersionService{
    @Autowired
    private VersionRepository versionRepository;

    @Override
    public String getCurrentVersion() {
        String currentVersion = versionRepository.getCurrentVersion();
        return currentVersion;
    }

}
