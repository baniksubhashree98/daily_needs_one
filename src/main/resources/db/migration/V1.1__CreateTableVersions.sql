CREATE TABLE versions (
version_name VARCHAR(25),
is_current BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY(version_name)
)
