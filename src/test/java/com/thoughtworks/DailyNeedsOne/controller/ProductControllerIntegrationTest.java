package com.thoughtworks.DailyNeedsOne.controller;

import com.thoughtworks.DailyNeedsOne.DailyNeedsOneApplication;
import com.thoughtworks.DailyNeedsOne.entity.Product;
import com.thoughtworks.DailyNeedsOne.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DailyNeedsOneApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ProductControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    public void before() {
        productRepository.deleteAll();
    }

    @AfterEach
    public void after() {
        productRepository.deleteAll();
    }

    @Test
    public void retrieveAllProducts() throws Exception {
        Product product = Product.builder()
//                .productId(1)
                .name("toothpaste")
                .priceInRupees(75)
                .build();
        productRepository.save(product);

        mockMvc.perform(get("/products")).andExpect(status().isOk()).andDo(print()).andExpect(content().json("[" + "{" + "'productId':" + product.getProductId() + ",'name':" + product.getName() + ",'priceInRupees':" + product.getPriceInRupees() + "}" + "]"));

    }
}
