package com.thoughtworks.DailyNeedsOne.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.DailyNeedsOne.entity.Product;
import com.thoughtworks.DailyNeedsOne.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = ProductController.class)
public class ProductControllerTest {

    @MockBean
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldFetchAllProducts() throws Exception {
        List<Product> products = new ArrayList<>();
        Product toothpaste = Product.builder()
                .productId(1)
                .name("toothpaste")
                .priceInRupees(75)
                .build();
        Product sunglasses = Product.builder()
                .productId(2)
                .name("sunglasses")
                .priceInRupees(450)
                .build();
        products.add(toothpaste);
        products.add(sunglasses);
        given(productService.getAllProducts()).willReturn(products);

        ResultActions response = mockMvc.perform(get("/products"));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()",
                        is(products.size())));
    }

}
