package com.thoughtworks.DailyNeedsOne.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.DailyNeedsOne.entity.Version;
import com.thoughtworks.DailyNeedsOne.service.VersionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = VersionController.class)
public class VersionControllerTest {
    @MockBean
    private VersionService versionService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetCurrentVersion() throws Exception {
        Version version = Version.builder()
                .versionName("v1")
                .isCurrent(true)
                .build();
        given(versionService.getCurrentVersion()).willReturn((version.getVersionName()));

        ResultActions response = mockMvc.perform(get("/version"));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.CurrentVersion", is(version.getVersionName())));
    }
}
