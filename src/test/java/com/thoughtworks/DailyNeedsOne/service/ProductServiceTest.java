package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Product;
import com.thoughtworks.DailyNeedsOne.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ProductServiceTest {
    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository productRepository;

    @Test
    void shouldRetrieveAllProducts() {
        List<Product> products = new ArrayList<>();
        Product toothpaste = Product.builder()
                .productId(1)
                .name("toothpaste")
                .priceInRupees(75)
                .build();
        Product sunglasses = Product.builder()
                .productId(2)
                .name("sunglasses")
                .priceInRupees(450)
                .build();
        products.add(toothpaste);
        products.add(sunglasses);
        when(productRepository.findAll()).thenReturn(products);
        List<Product> expectedProducts = List.of(toothpaste, sunglasses);

        List<Product> actualProducts = productService.getAllProducts();

        assertThat(expectedProducts).isEqualTo(actualProducts);
    }
}
