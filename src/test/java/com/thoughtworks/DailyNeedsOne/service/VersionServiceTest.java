package com.thoughtworks.DailyNeedsOne.service;

import com.thoughtworks.DailyNeedsOne.entity.Version;
import com.thoughtworks.DailyNeedsOne.repository.VersionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class VersionServiceTest {

    @Autowired
    private VersionService versionService;

    @MockBean
    private VersionRepository versionRepository;

    @Test
    void shouldFindPlaylistById() {
        Version version = Version.builder()
                .versionName("v1")
                .isCurrent(true)
                .build();
        when(versionRepository.getCurrentVersion()).thenReturn(version.getVersionName());
        String expectedCurrentVersion = version.getVersionName();

        String actualCurrentVersion = versionService.getCurrentVersion();

        assertEquals(expectedCurrentVersion, actualCurrentVersion);
    }

}
